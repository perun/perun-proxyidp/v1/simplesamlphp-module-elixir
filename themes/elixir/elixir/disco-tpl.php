<?php

declare(strict_types=1);

use SimpleSAML\Module\elixir\Disco;
use SimpleSAML\Module\perun\model\WarningConfiguration;
use SimpleSAML\Utils\HTTP;

$warningAccepted = false;

if (isset($_POST['accepted'])) {
    $warningAccepted = true;
}

$idpEntityId = null;
$authContextClassRef = null;

$continueUrl = $this->data[Disco::CONTINUE_URL];
$warningConfiguration = $this->data[Disco::WARNING] ?? null;

if (empty($warningConfiguration) || ! $warningConfiguration->isEnabled()) {
    HTTP::redirectTrustedURL($continueUrl);
}

$preventUserContinue = WarningConfiguration::WARNING_TYPE_ERROR === $warningConfiguration->getType();
if (!$preventUserContinue && $warningAccepted) {
    HTTP::redirectTrustedURL($continueUrl);
}

$this->data['header'] = $this->t('{elixir:disco:warning_header}');
$this->data['jquery'] = [
    'core' => true,
    'ui' => true,
    'css' => true,
];

$this->includeAtTemplateBase('includes/header.php');

$this->includeInlineTranslation('{elixir:disco:warning_title}', $warningConfiguration->getTitle());
$this->includeInlineTranslation('{elixir:disco:warning_text}', $warningConfiguration->getText());
if (WarningConfiguration::WARNING_TYPE_INFO === $warningConfiguration->getType()) {
    echo '<div class="alert alert-info">';
} elseif (WarningConfiguration::WARNING_TYPE_WARNING === $warningConfiguration->getType()) {
    echo '<div class="alert alert-warning">';
} elseif (WarningConfiguration::WARNING_TYPE_ERROR === $warningConfiguration->getType()) {
    echo '<div class="alert alert-danger">';
}
echo '<h4><strong>' . $this->t('{elixir:disco:warning_title}') . '</strong> </h4>';
echo  $this->t('{elixir:disco:warning_text}');
echo '</div>';
if (!$preventUserContinue) {
    echo '<form method="POST">';
    echo '<input class="btn btn-lg btn-primary btn-block" type="submit" name="accepted" value="' .
        $this->t('{elixir:disco:warning_continue}') . '" />';
    echo '</form>';
}

$this->includeAtTemplateBase('includes/footer.php');
